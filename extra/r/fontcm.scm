;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2021 Marek Felsoci
(define-module (extra r fontcm)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system r))

(define-public r-fontcm
  (package
    (name "r-fontcm")
    (version "1.1")
    (source
     (origin
       (method url-fetch)
       (uri (cran-uri "fontcm" version))
       (sha256
        (base32
         "1z6b4qdgj5vhvjqj90sm1hp0fffi1vxzvq71p0flxybzyb7d15la"))))
    (build-system r-build-system)
    (home-page "https://github.com/wch/fontcm")
    (synopsis "Computer Modern font for use with the @code{extrafont} package")
    (description #f)
    (license license:gpl2)))
