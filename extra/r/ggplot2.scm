;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2021 Marek Felsoci
(define-module (extra r ggplot2)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (gnu packages statistics))

(define-public r-ggplot2-git
  (package
   (inherit r-ggplot2)
   (version "git.bd50a551")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://github.com/tidyverse/ggplot2")
       (commit "bd50a5515516fc18ed9a6f13d074ba6016b34721")))
     (sha256
      (base32
       "0q8979vszrsm07zljl8icq6sykn4w4fzmqrsswq8xsgzpcq5hz6j"))))))
