;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2022 Marek Felsoci
(define-module (extra r ascii)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system r)
  #:use-module (gnu packages statistics))

(define-public r-ascii
  (package
   (name "r-ascii")
   (version "2.4")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "ascii" version))
     (sha256
      (base32 "0k675an7sl00bslx4yb6vvvgnp1kl244cfjljv632asqdm3rkmrv"))))
   (properties `((upstream-name . "ascii")))
   (build-system r-build-system)
   (propagated-inputs
    (list r-codetools r-digest r-survival))
   (home-page "https://github.com/mclements/ascii")
   (synopsis "Export R Objects to Several Markup Languages")
   (description
    "Coerce R object to 'asciidoc', 'txt2tags', 'restructuredText', 'org',
'textile' or 'pandoc' syntax.  Package comes with a set of drivers for
'Sweave'.")
   (license license:gpl2+)))
