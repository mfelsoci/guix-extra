;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2021 Marek Felsoci
(define-module (extra r ggpattern)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system r)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages cran))

(define-public r-gridpattern
  (package
   (name "r-gridpattern")
   (version "1.0.1")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "gridpattern" version))
     (sha256
      (base32
       "0ia0n5k72a3a2c2hhgg0ykmbbq42yr1jf81xvmqq28jd4nzvzqn1"))))
   (properties `((upstream-name . "gridpattern")))
   (build-system r-build-system)
   (propagated-inputs (list r-glue r-memoise r-png r-rlang r-sf))
   (native-inputs (list r-knitr))
   (home-page "https://trevorldavis.com/R/gridpattern/")
   (synopsis "'grid' Pattern Grobs")
   (description
    "This package provides grid grobs that fill in a user-defined area with various
patterns.  Includes enhanced versions of the geometric and image-based patterns
originally contained in the ggpattern package as well as original pch',
polygon_tiling', regular_polygon', rose', text', wave', and weave patterns plus
support for custom user-defined patterns.")
   (license license:expat)))

(define-public r-ggpattern
  (package
   (name "r-ggpattern")
   (version "1.0.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "ggpattern" version))
     (sha256
      (base32
       "09djvwdzvmqj6jk56vspbcvp598vkb9zvyvf83ym5mfp05yf3sf7"))))
   (properties `((upstream-name . "ggpattern")))
   (build-system r-build-system)
   (propagated-inputs (list r-ggplot2 r-glue r-gridpattern r-rlang r-scales))
   (native-inputs (list r-knitr))
   (home-page "https://github.com/coolbutuseless/ggpattern")
   (synopsis "'ggplot2' Pattern Geoms")
   (description
    "This package provides ggplot2 geoms filled with various patterns.  Includes a
patterned version of every ggplot2 geom that has a region that can be filled
with a pattern.  Provides a suite of ggplot2 aesthetics and scales for
controlling pattern appearances.  Supports over a dozen builtin patterns (every
pattern implemented by gridpattern') as well as allowing custom user-defined
patterns.")
   (license license:expat)))
