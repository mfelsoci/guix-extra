;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2023, 2024 Marek Felsoci

(define-module (extra optitrust optitrust)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system dune)
  #:use-module (guix build-system ocaml)
  #:use-module (gnu packages)
  #:use-module (gnu packages ocaml)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages patchutils)
  #:use-module (gnu packages pkg-config)
  #:use-module (extra optitrust dependencies))

(define-public optitrust
  (let ((release "0")
        (commit "e5d2ba8b01211858cd3c70da597fb2656a357362")
        (revision "0"))
    (package
     (name "ocaml-optitrust")
     (version (git-version release revision commit))
     (supported-systems '("x86_64-linux"))
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/charguer/optitrust")
             ;; FIXME: branch `marek' --> branch `main' after merge.
             (commit commit)))
       (sha256
        (base32
         "09fb54wy7l9fi342q6w5las5d3z1j87h9zwd2hipdvixd5dna64y"))))
     (build-system ocaml-build-system)
     (arguments
      '(#:phases
        (modify-phases
         %standard-phases
         ;; There is no `configure' script.
         (delete 'configure)
         (add-before
          'build 'patch-paths
          (lambda*
           (#:key inputs outputs #:allow-other-keys)
           (let*
               ((prefixdir (assoc-ref outputs "out"))
                (gcc-lib-includes (assoc-ref inputs "gcc:lib"))
                ;; Determine the version of GCC used by the builder.
                (gcc-version
                 (call-with-values
                     (lambda () (package-name->name+version gcc-lib-includes))
                   (lambda (_ version)
                     ;; Initially, `version' contains the '-lib' suffix, so we
                     ;; have to drop it.
                     (string-drop-right version 4))))
                (compcert-dir (string-append prefixdir "/lib/compcert"))
                (libdir
                 (string-append
                  (assoc-ref outputs "out") "/lib/ocaml/site-lib")))
             (setenv "C_INCLUDE_PATH"
                     (string-append
                      (getenv "C_INCLUDE_PATH") ":" gcc-lib-includes
                      "/lib/gcc/x86_64-unknown-linux-gnu/" gcc-version
                      "/include"))
             (setenv "CPLUS_INCLUDE_PATH"
                     (string-append
                      (getenv "CPLUS_INCLUDE_PATH") ":" gcc-lib-includes
                      "/lib/gcc/x86_64-unknown-linux-gnu/" gcc-version
                      "/include"))
             ;; Standardize the name of the `test' target.
             (substitute* "Makefile"
                          (("tests:") "test:"))
             ;; Patch output paths.
             (mkdir-p (string-append prefixdir "/bin"))
             (mkdir-p compcert-dir)
             (substitute* "src/c/compcert_parser/Config.ml"
                          (("/usr/local/lib/compcert") compcert-dir))
             (substitute* "Makefile"
                          (("\\$\\(OPTITRUST_PREFIX\\)") prefixdir))
             (substitute*
              "tests/Tests.Makefile"
              (("\\$\\(shell echo `opam config var prefix`\\)") prefixdir))
             (substitute* "Makefile"
                          (("dune install -p \\$\\(THIS\\)")
                           (string-append
                            "dune install -p $(THIS) --prefix "
                            prefixdir " --libdir " libdir))))))
         (add-after 'build 'install-compcert
                    (lambda*
                     (#:key outputs #:allow-other-keys)
                     (let
                         ((compcert-dir
                           (string-append
                            (assoc-ref outputs "out") "/lib/compcert/")))
                       (for-each (lambda (file)
                                   (install-file file compcert-dir))
                                 (find-files
                                  "src/c/compcert_parser/include" "\\.h")))))
         (replace 'check
                  (lambda _
                    (invoke "./tester" "case_studies/matmul/matmul.ml"))))))
     (inputs (list dune clang meld zlib))
     (native-inputs
      `(("pkg-config" ,pkg-config)
        ("gcc:lib" ,gcc "lib")))
     (propagated-inputs (list ocaml-menhir-2021 ocaml-pprint ocaml-clangml
ocaml-base64 ocamlbuild ocaml-graph-mf))
     (home-page "http://www.chargueraud.org/research/2022/sc_optitrust/")
     (synopsis "OptiTrust: an Interactive Framework for Source-to-Source
Transformations")
     (description
      "OptiTrust can be used for producing a high-performance code via a
transformation script, rather than by writing optimized code by hand.")
     (license license:expat))))
