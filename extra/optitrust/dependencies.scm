;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2023, 2024 Marek Felsoci

(define-module (extra optitrust dependencies)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system dune)
  #:use-module (guix build-system ocaml)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages ocaml)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages base))

(define-public ocaml-sexplib0-0.16
  (package
   (inherit ocaml-sexplib0)
   (name "ocaml-sexplib0")
   (version "0.16.0")
   (home-page "https://github.com/janestreet/sexplib0")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url home-page)
                  (commit (string-append "v" version))))
            (file-name (git-file-name name version))
            (sha256
             (base32
              "14avixpsx3j2qrihcmlyam6m2ziw2n9azb5lbjzsn1ipdqrza6n1"))))))

(define ppxlib-0.32
  (package
   (inherit ocaml-ppxlib)
   (name "ocaml-ppxlib")
   (version "0.32.1")
   (source
    (origin
     (method url-fetch)
     (uri
      (string-append
       "https://github.com/ocaml-ppx/ppxlib/releases/download/"
       version "/ppxlib-" version ".tbz"))
     (sha256
      (base32
       "0zwxq22xasy8jr0lp8qd3x9qcgfjbq5adg2q7zgz7d68n6ydiflx"))))))

(define fix-dependencies-of-ocaml-ppxlib-0.32
  (package-input-rewriting `((,ocaml-sexplib0 . ,ocaml-sexplib0-0.16))))

(define-public ocaml-ppxlib-0.32
  (fix-dependencies-of-ocaml-ppxlib-0.32 ppxlib-0.32))

(define ppx-inline-test-0.16
  (package
   (inherit ocaml-ppx-inline-test)
   (name "ocaml-ppx-inline-test")
   (version "0.16.1")
   (home-page "https://github.com/janestreet/ppx_inline_test")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url (string-append home-page ".git"))
           (commit (string-append "v" version))))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "1syrlng8c1c4w6qs7c91vpwvzg6ij65ps8nbgw8499vpgqcvjnnk"))))))

(define fix-dependencies-of-ocaml-inline-test-0.16
  (package-input-rewriting
   `((,ocaml-sexplib0 . ,ocaml-sexplib0-0.16)
     (,ocaml-ppxlib . ,ocaml-ppxlib-0.32))))

(define-public ocaml-ppx-inline-test-0.16
  (fix-dependencies-of-ocaml-inline-test-0.16 ppx-inline-test-0.16))

(define ppx-deriving-6
  (package
   (inherit ocaml-ppx-deriving)
   (name "ocaml-ppx-deriving")
   (version "6.0.2")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/ocaml-ppx/ppx_deriving")
           (commit (string-append "v" version))))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "0np3cqdgzl99shyyi4rx4har8habfb2jj2r3cly8rqrrmp2mvz8l"))))))

(define fix-dependencies-of-ocaml-ppx-deriving-6
  (package-input-rewriting
   `((,ocaml-sexplib0 . ,ocaml-sexplib0-0.16)
     (,ocaml-ppxlib . ,ocaml-ppxlib-0.32))))

(define-public ocaml-ppx-deriving-6
  (fix-dependencies-of-ocaml-ppx-deriving-6 ppx-deriving-6))

(define ppx-expect-0.16
  (package
   (inherit ocaml-ppx-expect)
   (name "ocaml-ppx-expect")
   (version "0.16.0")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/janestreet/ppx_expect")
           (commit (string-append "v" version))))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "0zrfxzfdxzbhxbz5079cyi80r4217syxzbhj1drcgjczgd29p70z"))))))

(define fix-dependencies-of-ocaml-ppx-expect-0.16
  (package-input-rewriting
   `((,ocaml-sexplib0 . ,ocaml-sexplib0-0.16)
     (,ocaml-ppxlib . ,ocaml-ppxlib-0.32)
     (,ocaml-ppx-inline-test . ,ocaml-ppx-inline-test-0.16))))

(define-public ocaml-ppx-expect-0.16
  (fix-dependencies-of-ocaml-ppx-expect-0.16 ppx-expect-0.16))

(define metapp
  (let ((release "0.4.4")
        (commit "26e20714348607cec5d978808e666ed414ba092b")
        (revision "14"))
    (package
     (name "ocaml-metapp")
     (version (git-version release revision commit))
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/thierry-martinez/metapp")
             (commit commit)))
       (sha256
        (base32
         "1xbi0xh24qp46pq0imahmkzhgzwn8vi77lzns009zdnmyqafw7p6"))))
     (build-system dune-build-system)
     (arguments
      `(#:package "metapp"))
     (propagated-inputs
      (list ocaml-stdcompat ocaml-ppxlib ocaml-findlib ocaml-odoc))
     (home-page "https://github.com/thierry-martinez/metapp")
     (synopsis "Meta-preprocessor for OCaml")
     (description
      "Meta-preprocessor for OCaml: extends the language with [%meta ... ]
construction where ... stands for OCaml code evaluated at compile-time.")
     (license license:bsd-2))))

(define fix-dependencies-of-ocaml-metapp
  (package-input-rewriting
   `((,ocaml-sexplib0 . ,ocaml-sexplib0-0.16)
     (,ocaml-ppxlib . ,ocaml-ppxlib-0.32)
     (,ocaml-ppx-inline-test . ,ocaml-ppx-inline-test-0.16)
     (,ocaml-ppx-expect . ,ocaml-ppx-expect-0.16))))

(define-public ocaml-metapp
   (fix-dependencies-of-ocaml-metapp metapp))

(define-public ocaml-metaquot
  (package
   (name "ocaml-metaquot")
   (version "0.5.2")
   (source
    (origin
     (method url-fetch)
     (uri
      (string-append
       "https://github.com/thierry-martinez/metaquot/releases/download/v"
       version "/metaquot." version ".tar.gz"))
     (sha256
      (base32
       "012k8zs4vya7vjkmkshkbnxjbxqn5j7fp83vqi8lg1zfsiw4shc4"))))
   (build-system dune-build-system)
   (propagated-inputs (list ocaml-stdcompat ocaml-ppxlib-0.32 ocaml-metapp))
   (home-page "https://github.com/thierry-martinez/metaquot")
   (synopsis "OCaml syntax extension for quoting code")
   (description "metaquot allows to quote OCaml code.")
   (license license:bsd-2)))

(define-public ocaml-traverse
  (package
   (name "ocaml-traverse")
   (version "0.3.0")
   (source
    (origin
     (method url-fetch)
     (uri
      (string-append 
       "https://github.com/thierry-martinez/traverse/archive/v"
       version ".tar.gz"))
     (sha256
      (base32
       "19nam6kxccqhxs0sb41bd01agv7f1595jiz3zlysscfv8nmfm3za"))))
   (build-system dune-build-system)
   (propagated-inputs (list ocaml-stdcompat ocaml-metapp ocaml-metaquot))
   (home-page "https://github.com/thierry-martinez/traverse")
   (synopsis "Traversable data structures with applicative functors")
   (description
    "Definition of applicative functors with instances for traversable data
structures")
   (license license:bsd-2)))

(define-public ocaml-refl
  (package
   (name "ocaml-refl")
   (version "0.4.1")
   (source
    (origin
     (method url-fetch)
     (uri
      (string-append
       "https://github.com/thierry-martinez/refl/releases/download/v"
       version "/refl." version ".tar.gz"))
     (sha256
      (base32
       "1qddwy1zhg1vnl7h6nw44qn8173zsi4z8ai9wksh19b33c97ps53"))))
   (build-system dune-build-system)
   (propagated-inputs (list ocaml-stdcompat
                            ocaml-fix
                            ocaml-ppxlib-0.32
                            ocaml-metapp
                            ocaml-metaquot
                            ocaml-traverse))
   (home-page "https://github.com/thierry-martinez/refl")
   (synopsis "PPX deriver for reflection")
   (description "PPX deriver for reflection")
   (license license:bsd-2)))

(define-public ocaml-pattern
  (package
   (name "ocaml-pattern")
   (version "0.3.2")
   (source
    (origin
     (method url-fetch)
     (uri
      (string-append
       "https://github.com/thierry-martinez/pattern/releases/download/v"
       version "/pattern." version ".tar.gz"))
     (sha256
      (base32
       "15jyv4ni6lv3xjz4y4x92pqzvviq4mb02q265bd7rbb0f3vlf5aa"))))
   (build-system dune-build-system)
   (propagated-inputs (list ocaml-metapp
                            ocaml-metaquot
                            ocaml-refl
                            ocaml-migrate-parsetree
                            ocaml-stdcompat))
   (home-page "https://github.com/thierry-martinez/pattern")
   (synopsis "Run-time patterns that explain match failures")
   (description
    "pattern is a PPX extension that generates functions from patterns that
explain match failures by returning the common context and the list of
differences between a pattern and a value.")
   (license license:bsd-2)))

(define-public ocaml-redirect
  (package
   (name "ocaml-redirect")
   (version "0.2.1")
   (source
    (origin
     (method url-fetch)
     (uri
      (string-append
       "https://github.com/thierry-martinez/redirect/releases/download/v"
       version "/redirect-" version ".tar.gz"))
     (sha256
      (base32
       "1wbp6xr9nhdnh1i0ga0w8cbmqmn2yrfw2fmwhma0lf4la61r74b4"))))
   (build-system dune-build-system)
   (propagated-inputs (list ocaml-stdcompat))
   (home-page "https://github.com/thierry-martinez/redirect")
   (synopsis "Redirect channels")
   (description "Redirect channels to files, strings...")
   (license license:bsd-2)))

(define-public ocaml-codoc
  (package
   (name "ocaml-codoc")
   (version "1.0.1")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://github.com/thierry-martinez/ocamlcodoc")
       (commit "4c2b4582ed3f195dd9bf87f87e868dd0377f2366")))
     (sha256
      (base32
       "10xx15lhschjx8i798ch630xd790biw1x9sx8zxmn0gscx1vi627"))))
   (build-system dune-build-system)
   (propagated-inputs (list ocaml-cmdliner ocaml-redirect ocaml-stdcompat))
   (home-page "https://github.com/thierry-martinez/ocamlcodoc")
   (properties `((upstream-name . "ocamlcodoc")))
   (synopsis "Extract test code from doc-comments")
   (description
    "ocamlcodoc extracts the preformatted source code in OCaml documentation
comments, i.e.  the code delimited by {[ ... ]} in comments delimited by (** ...
*).  A typical usage is to write examples in documentation comments that can be
extracted and tested.")
   (license #f)))

(define-public ocaml-clangml
  (package
   (name "ocaml-clangml")
   (version "4.8.0")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://github.com/thierry-martinez/clangml")
       (commit (string-append "v" version))))
     (sha256
      (base32
       "1pnbwnbbkrq229kr4l033h6vjmk47rbp03jzx2p320x0cjcqfk13"))))
   (build-system ocaml-build-system)
   (arguments
    '(#:phases
      (modify-phases
       %standard-phases
       (add-before
        'configure 'patch-paths
        (lambda* (#:key outputs #:allow-other-keys)
                 (let
                     ((prefixdir (assoc-ref outputs "out"))
                      (libdir
                       (string-append
                        (assoc-ref outputs "out") "/lib/ocaml/site-lib")))
                   ;; Fix paths to Shell interpreter.
                   (substitute* "configure"
                                (("CONFIG_SHELL-/bin/sh")
                                 (string-append "CONFIG_SHELL-" (which "sh")))
                                (("SHELL = /bin/sh")
                                 (string-append "SHELL = " (which "sh"))))
                   ;; Fix output paths.
                   (substitute* "Makefile.am"
                                (("\\$\\(DUNE\\) install")
                                 (string-append
                                  "$(DUNE) install --prefix " prefixdir
                                  " --libdir " libdir)))
                   (substitute* "Makefile.in"
                                (("\\$\\(DUNE\\) install")
                                 (string-append
                                  "$(DUNE) install --prefix " prefixdir
                                  " --libdir " libdir)))))))
      ;; FIXME: Find a way to indicate to the compiler where to find `stddef.h'.
      #:tests? #f))
   (inputs
    (list dune which clang))
   (propagated-inputs
    (list ocaml-ppxlib-0.32
          ocaml-refl
          ocaml-stdcompat
          ocaml-migrate-parsetree
          ;; For tests.
          ocaml-pattern
          ocaml-redirect
          ocaml-codoc))
   (home-page "https://github.com/thierry-martinez/clangml")
   (synopsis "clangml provides bindings for all versions of Clang, from 3.4 to
15.0.0.")
   (description
    "This library is a complete rewritting of the previous clangml (clangml
versions <4.0.0): the bindings now rely on automatically generated C stubs to
libclang, with some extensions when libclang is incomplete. Contrary to old
clangml versions, the versions of clangml from 4.0.0 are independent from the
version of the Clang library: any version of clangml from 4.0.0 can be built
with any version of the Clang library in the supported interval. Currently, all
versions of Clang, from 3.4 to 15.0.0, are supported.")
   (license license:bsd-2)))

(define-public ocaml-menhir-2021
  (package
   (inherit ocaml-menhir)
   (name "ocaml-menhir")
   (version "20210419")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://gitlab.inria.fr/fpottier/menhir.git")
           (commit version)))
     (file-name (git-file-name name version))
     (sha256
      (base32 "0jcbr7s3iwfr7xxfybs3h407g76yfp5yq5r9i0wg2ahvvbqh03ky"))))))

(define-public ocaml-graph-mf
  (package
   (inherit ocaml-graph)
   (name "ocaml-graph-mf")
   (version "2.1.0")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://github.com/felsocim/ocamlgraph")
       (commit "052d5b9f363ccc5bf9cea5227d7895d2ea9eb41f")))
     (sha256
      (base32
       "1izd7zg0jcskbc41dpqn7iz097nnrj0wr5yjcmkj84zc7f9lz6kk"))))
   (build-system dune-build-system)
   (arguments
    `(#:install-target "install"))
   (inputs (list lablgtk ocaml-graphics ocaml-stdlib-shims))))

