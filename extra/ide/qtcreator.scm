;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2019 Marek Felsoci

(define-module (extra ide qtcreator)
  #:use-module (guix)
  #:use-module (guix git-download)
  #:use-module (guix licenses)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages python)
  #:use-module (gnu packages vim)
  #:use-module (guix utils)
  #:use-module (srfi srfi-1))

(define-public qtcreator
  (package
   (name "qtcreator")
   (version "4.11.0")
   (home-page "https://code.qt.io/cgit/qt-creator/qt-creator.git")
   (synopsis "A cross-platform Qt integrated development environment.")
   (description "Qt Creator is a cross-platform C++, JavaScript and QML
integrated development environment which is part of the SDK for the Qt GUI
application development framework. It includes a visual debugger and an
integrated GUI layout and forms designer. The editor's features include syntax
highlighting and autocompletion.")
   (license lgpl3)
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url home-page)
       (commit "017ed74400de56b99c6fddb6f4691ba65d35695e")
       (recursive? #t)))
     (sha256
      (base32
       "1w5s9sk8s9z3hhs4v2mcgj4rvl6jq3pmam6b07vmzbz17hvsxnin"))))
   (build-system cmake-build-system)
   (arguments
    '(#:configure-flags (list (string-append "-DPYTHON_EXECUTABLE="
                                             (assoc-ref %build-inputs "python3")
                                             "/bin/python3"))
      #:phases
      (modify-phases %standard-phases
                     (add-before 'configure 'disable-sqlite-and-clang
                                 (lambda _
                                   ;; FIXME: Nor SQLite nor Clang extension does
                                   ;; compile correctly.
                                   (substitute* "src/libs/CMakeLists.txt"
                                     (("add_subdirectory\\(sqlite\\)")
                                       "# add_subdirectory(sqlite)")
                                     (("add_subdirectory\\(clangsupport\\)")
                                       "# add_subdirectory(clangsupport)")) #t))
                     (add-after 'install 'create-shortcut
                                (lambda _
                                  ;; Install the '*.desktop' file.
                                  (install-file
                                   (string-append
                                    "../source/dist/"
                                    "org.qt-project.qtcreator.desktop")
                                   (string-append
                                    (assoc-ref %outputs "out")
                                    "/share/applications"))

                                  ;; Install icons in various resolutions.
                                  (for-each (lambda (resolution)
                                              (mkdir-p
                                               (string-append
                                                (assoc-ref %outputs "out")
                                                "/share/icons/hicolor/"
                                                resolution
                                                "/apps"))
                                              (copy-file
                                               (string-append
                                                "../source/src/app/"
                                                "qtcreator.xcassets/"
                                                "qtcreator.appiconset/icon_"
                                                resolution
                                                ".png")
                                               (string-append
                                                (assoc-ref %outputs "out")
                                                "/share/icons/hicolor/"
                                                resolution
                                                "/apps/QtProject-qtcreator.png")
                                               ))
                                            (list
                                             "16x16"
                                             "32x32"
                                             "128x128"
                                             "256x256"
                                             "512x512")) #t))
                     ;; FIXME: Makes the build/installation fail for a currently
                     ;; unknown reason.
                     (delete 'validate-runpath))
      #:tests? #f)) ;; FIXME: Google test package not recognized!
   (inputs
    `(("perl", perl)
      ("python" ,python-2)
      ("python3" ,python)
      ("xxd" ,xxd)
      ("llvm" ,llvm)
      ("qt5" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)
      ("qtsvg" ,qtsvg)
      ("qtxmlpatterns" ,qtxmlpatterns)
      ("qttools" ,qttools)
      ("qtscript" ,qtscript)
      ("qtserialport" ,qtserialport)))))
