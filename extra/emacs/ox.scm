;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2024 Marek Felsoci

(define-module (extra emacs ox)
  #:use-module (guix)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system emacs)
  #:use-module (gnu packages)
  #:use-module (guix utils)
  #:use-module (srfi srfi-1))

(define-public emacs-ox-latex-subfigure
  (let ((release "0.0.2")
        (commit "c4487689309dddff3228603754b69ab381cfa5dc")
        (revision "32"))
    (package
     (name "emacs-ox-latex-subfigure")
     (version (git-version release revision commit))
     (source
      (origin
       (method git-fetch)
       (uri
        (git-reference
         (url "https://github.com/linktohack/ox-latex-subfigure")
         (commit "c4487689309dddff3228603754b69ab381cfa5dc")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "17p42zv2slnfahrh9ln3rrh3fjnh2bk4j4fxljn589cgv0wblwqc"))))
     (build-system emacs-build-system)
     (home-page "https://github.com/linktohack/ox-latex-subfigure")
     (synopsis
      "Turn table into subfigure")
     (description
      "Turn table into subfigure. More or less a hack atm, easy to incorporate
to org-mode though.")
     (license license:gpl3+))))

