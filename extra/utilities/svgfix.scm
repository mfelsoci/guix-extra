;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2021 Marek Felsoci

(define-module (extra utilities svgfix)
  #:use-module (guix)
  #:use-module (guix git-download)
  #:use-module (guix licenses)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages)
  #:use-module (gnu packages xml)
  #:use-module (guix utils)
  #:use-module (srfi srfi-1))

(define-public svgfix
  (package
   (name "svgfix")
   (version "1.0")
   (home-page "https://gitlab.inria.fr/mfelsoci/svgfix.git")
   (synopsis "Fix the attributes of SVG images controlling the way they are
displayed on HTML pages.")
   (description "Small utility program to add or fix the 'viewBox' and
'preserveAspectRatio' attributes of SVG images to ensure their initial
dimensions defined in 'width' and 'height' are respected when displayed on an
HTML page.")
   (license gpl2)
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url home-page)
       (commit "26ed2a3dac5b9712e48da3a2bca794107c84fa72")))
     (sha256
      (base32
       "01a5a6ilx0vjwpzh5y6cq2zl85sq8h7rjdikk6x2c5q41q99fcd6"))))
   (build-system cmake-build-system)
   (arguments
    '(#:tests? #f)) ;; There is no test suite yet.
   (inputs
    `(("libxml2" ,libxml2)))))
